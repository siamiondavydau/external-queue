# External RabbitMQ queue for Laravel/Lumen and Yii2.

## Installing

```bash
composer require siamiondavydau/external-queue
```

## Usage
Add to environment variables
```
EXTERNAL_RABBITMQ_HOST=
EXTERNAL_RABBITMQ_PORT=
EXTERNAL_RABBITMQ_LOGIN=
EXTERNAL_RABBITMQ_PASSWORD=
```
Send to the "queue_name" queue
```php
(new Siamiondavydau\ExternalQueue\ExternalQueue())
    ->add(
        'queue_name',
        'App\Jobs\ExampleJob',
        $data
    );
```
### Integrations
**Laravel**

Register service provider in `config/app.php`

```php
'providers' => [
    \Siamiondavydau\ExternalQueue\Laravel\ExternalQueueServiceProvider\ExternalQueueServiceProvider::class,
]
```

**Lumen**

Register service provider in `bootstrap/app.php`
```php
$app->register(\Siamiondavydau\ExternalQueue\Laravel\ExternalQueueServiceProvider\ExternalQueueServiceProvider::class);
```

Running the queue worker
```bash
php artisan external-queue:work --queue=queue_name
```

**Yii2**

Register module in `config/console.php`
```php
$config = [
    'bootstrap' => [
        'external-queue',
    ],
    'modules' => [
        'external-queue' => [
            'class' => \Siamiondavydau\ExternalQueue\Yii\Module::class,
        ],
    ],
];
```

Running the queue worker
```bash
php yii external-queue/listener/work --queue=queue_name
```
