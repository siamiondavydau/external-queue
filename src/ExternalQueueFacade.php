<?php

namespace Siamiondavydau\ExternalQueue;

class ExternalQueueFacade
{
    /**
     * @param string $queueName
     * @param string $job
     * @param array $item
     */
    public static function add(string $queueName, string $job, array $item)
    {
        (new ExternalQueue())->add($queueName, $job, $item);
    }

    /**
     * @param string $queueName
     * @param string $job
     * @param array $item
     * @param int $seconds
     */
    public static function addWithDelay(string $queueName, string $job, array $item, int $seconds)
    {
        (new ExternalQueue())->addWithDelay($queueName, $job, $item, $seconds);
    }

    /**
     * @param string $queueName
     * @param string $job
     * @param array $item
     */
    public static function batchAdd(string $queueName, string $job, array $item)
    {
        (new ExternalQueue())->add($queueName, $job, $item);
    }

    /**
     * @param string $queueName
     * @param string $job
     * @param array $item
     * @param int $seconds
     */
    public static function batchAddWithDelay(string $queueName, string $job, array $item, int $seconds)
    {
        (new ExternalQueue())->batchAddWithDelay($queueName, $job, $item, $seconds);
    }
}
