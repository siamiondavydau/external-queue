<?php

namespace Siamiondavydau\ExternalQueue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class ExternalQueue
{
    /**
     * @return AMQPStreamConnection
     */
    protected function connection(): AMQPStreamConnection
    {
        if (is_null(getenv('EXTERNAL_RABBITMQ_HOST'))) {
            throw new \Exception("EXTERNAL_RABBITMQ_HOST not set.");
        }

        return new AMQPStreamConnection(
            getenv('EXTERNAL_RABBITMQ_HOST'),
            getenv('EXTERNAL_RABBITMQ_PORT'),
            getenv('EXTERNAL_RABBITMQ_LOGIN'),
            getenv('EXTERNAL_RABBITMQ_PASSWORD')
        );
    }

    /**
     * @param string $queueName
     * @param string $job
     * @param array $item
     */
    public function add(string $queueName, string $job, array $item): void
    {
        $connection = $this->connection();

        $channel = $connection->channel();

        $channel->queue_declare(
            $queueName,
            false,
            true,
            false,
            false
        );

        $message = new AMQPMessage(json_encode([
            'job' => $job,
            'data' => $item,
        ]));

        $channel->basic_publish($message, '', $queueName);

        $channel->close();
        $connection->close();
    }

    /**
     * @param string $queueName
     * @param string $job
     * @param array $item
     * @param int $seconds
     */
    public function addWithDelay(string $queueName, string $job, array $item, int $seconds): void
    {
        $connection = $this->connection();

        $channel = $connection->channel();

        $channel->queue_declare(
            $this->getDelayQueueName($queueName, $seconds),
            false,
            true,
            false,
            false,
            false,
            new AMQPTable([
                'x-dead-letter-exchange' => '',
                'x-dead-letter-routing-key' => $queueName,
                'x-message-ttl' =>	$seconds * 1000,
            ])
        );

        $message = new AMQPMessage(json_encode([
            'job' => $job,
            'data' => $item,
        ]));

        $channel->basic_publish($message, '', $this->getDelayQueueName($queueName, $seconds));

        $channel->close();
        $connection->close();
    }

    /**
     * @param string $queueName
     * @param string $job
     * @param array $items
     */
    public function batchAdd(string $queueName, string $job, array $items): void
    {
        $connection = $this->connection();

        $channel = $connection->channel();

        $channel->queue_declare(
            $queueName,
            false,
            true,
            false,
            false
        );

        foreach ($items as $item) {
            $message = new AMQPMessage(json_encode([
                'job' => $job,
                'data' => $item,
            ]));

            $channel->batch_basic_publish($message, '', $queueName);
        }
        $channel->publish_batch();

        $channel->close();
        $connection->close();
    }

    /**
     * @param string $queueName
     * @param string $job
     * @param array $items
     * @param int $seconds
     */
    public function batchAddWithDelay(string $queueName, string $job, array $items, int $seconds): void
    {
        $connection = $this->connection();

        $channel = $connection->channel();

        $channel->queue_declare(
            $this->getDelayQueueName($queueName, $seconds),
            false,
            true,
            false,
            false,
            false,
            new AMQPTable([
                'x-dead-letter-exchange' => '',
                'x-dead-letter-routing-key' => $queueName,
                'x-message-ttl' =>	$seconds * 1000,
            ])
        );

        foreach ($items as $item) {
            $message = new AMQPMessage(json_encode([
                'job' => $job,
                'data' => $item,
            ]));

            $channel->batch_basic_publish($message, '', $this->getDelayQueueName($queueName, $seconds));
        }
        $channel->publish_batch();

        $channel->close();
        $connection->close();
    }

    /**
     * @param string $queueName
     * @return bool
     */
    public function listen(string $queueName): bool
    {
        $connection = $this->connection();

        $channel = $connection->channel();
        $channel->basic_qos(null, 1, false);
        $channel->queue_declare($queueName, false, true, false, false);
        $channel->basic_consume($queueName, '', false, false, false, false, [$this, 'handle']);

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();

        return true;
    }

    /**
     * @param AMQPMessage $message
     * @return bool
     */
    public function handle(AMQPMessage $message): bool
    {
        $array = json_decode($message->getBody(), true);

        $data = new Message($array['job'], $array['data']);

        if (!class_exists($data->getJob())) {
            throw new \Exception('Invalid job: ' . $data->getJob());
        }

        $job = $data->getJob();
        $job = new $job($data->getData());

        $job->handle();

        $message->getChannel()->basic_ack($message->getDeliveryTag());

        if (memory_get_usage(false) / 1024 / 1024 > 300) {
            echo 'memory limit';
            die();
        }

        return true;
    }

    /**
     * @param string $queueName
     * @param int $seconds
     * @return string
     */
    protected function getDelayQueueName(string $queueName, int $seconds): string
    {
        return $queueName . ".{$seconds}.x.delay";
    }
}
