<?php

namespace Siamiondavydau\ExternalQueue\Laravel\Console\Commands;

use Illuminate\Console\Command;
use Siamiondavydau\ExternalQueue\ExternalQueue;

class Work extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'external-queue:work {--queue=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'External queue work';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $queue = $this->option('queue');

        if (is_null($queue)) {
            throw new \Exception('--queue option is required.');
        }

        $service = new ExternalQueue();
        $service->listen($queue);
    }
}
