<?php

namespace Siamiondavydau\ExternalQueue\Laravel;

use Illuminate\Support\ServiceProvider;
use Siamiondavydau\ExternalQueue\Laravel\Console\Commands\Work;

class ExternalQueueServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            Work::class,
        ]);
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {

    }
}
