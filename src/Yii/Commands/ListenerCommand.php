<?php

namespace Siamiondavydau\ExternalQueue\Yii\Commands;

use Siamiondavydau\ExternalQueue\ExternalQueue;
use yii\console\Controller;

class ListenerCommand extends Controller
{
    /**
     * @var string
     */
    public $queue;

    /**
     * @param string $actionID
     * @return array|string[]
     */
    public function options($actionID)
    {
        $options = parent::options($actionID);
        $options[] = 'queue';

        return $options;
    }

    /**
     * @return void
     */
    public function actionWork()
    {
        if (is_null($this->queue)) {
            throw new \Exception('--queue option is required.');
        }

        $service = new ExternalQueue();
        $service->listen($this->queue);
    }
}
