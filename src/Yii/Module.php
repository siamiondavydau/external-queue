<?php

namespace Siamiondavydau\ExternalQueue\Yii;

use Siamiondavydau\ExternalQueue\Yii\Commands\ListenerCommand;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Module as BaseModule;

class Module extends BaseModule implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'vendor\siamiondavydau\external-queue\src\Yii\Commands';

    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $this->controllerMap['listener'] = ListenerCommand::class;
    }
}
