<?php

namespace Siamiondavydau\ExternalQueue;

class Message
{
    /**
     * @var string
     */
    protected $job;

    /**
     * @var array
     */
    protected $data;

    /**
     * Message constructor.
     * @param string $job
     * @param array $data
     */
    public function __construct(string $job, array $data)
    {
        $this->job = $job;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getJob(): string
    {
        return $this->job;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return false|string
     */
    public function __toString()
    {
        return json_encode([
            'job' => $this->job,
            'data' => $this->data,
        ]);
    }
}
